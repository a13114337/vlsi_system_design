module ALU ( enable_execute, OverFlow, alu_result, src1, src2, OP ) ;
   parameter ISize = 32 ;
   parameter OPSize = 5 ;

   input enable_execute ;
   input [ ISize - 1 : 0 ] src1 ;
   input [ ISize - 1 : 0 ] src2 ;
   input [ 4 : 0 ] OP ;// operation code 
   output [ ISize - 1 : 0 ] alu_result ;
   output OverFlow ;

   reg [ ISize - 1 : 0 ] alu_result ;
   reg [ 2 * ISize - 1 : 0 ] temp ;
   reg OverFlow ;
   integer i  ;

   always@( enable_execute or src1 or src2 or OP) begin
     OverFlow = 0 ;
       case ( OP )
         5'b00000 :
         begin
           alu_result = src1 + src2 ;  // ADD
           if ( ( alu_result[31] == 0 && src1[31] == 1 && src2[31] == 1 ) ||
                ( alu_result[31] == 1 && src1[31] == 0 && src2[31] == 0 ) )
             OverFlow = 1 ;
           else
             OverFlow = 0 ;
         end
      
         5'b00001 :
         begin
           alu_result = src1 - src2 ;  // sub
           if ( ( alu_result[31] == 0 && src1[31] == 1 && src2[31] == 0 ) ||
                ( alu_result[31] == 1 && src1[31] == 0 && src2[31] == 1 ) )
             OverFlow = 1 ;
           else
             OverFlow = 0 ;
         end

         5'b00010 : alu_result = src1 & src2 ;  // and
         
         5'b00100 : alu_result = src1 | src2 ;  // or
         
         5'b00011 : alu_result = src1 ^ src2 ;  // or

         5'b01001 : alu_result = src1 >> src2 ; // right shift

         5'b01000 : alu_result = src1 << src2 ; // left shift       
         
         5'b01011: 
         begin 
            temp = { src1, src1 } ;
            i = src2 & 5'b11111 ;
            temp = temp >> i ;
            alu_result = temp[ 31 : 0 ] ;
         end

         default : alu_result = 0 ; 
       endcase
   end  

endmodule
