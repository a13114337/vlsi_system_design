module RegFile( clk, rst, Write, Read, Write_ADDR, Read_ADDR1, Read_ADDR2, Write_Data, Read_Data1, Read_Data2 ) ;


  parameter ADSize = 5 ;    // 5 bits
  parameter REGSize = 32 ;  // 32 bits  
  parameter DASize = 32 ;   // 32 bits 

  input clk, rst ;
  input [ ADSize - 1 : 0 ] Write_ADDR, Read_ADDR1, Read_ADDR2;
  input [ DASize - 1 : 0 ] Write_Data  ;
  input Write ;
  input Read ;
  output [ DASize - 1 : 0 ] Read_Data1, Read_Data2 ;
   

  reg [ DASize - 1 : 0 ] Read_Data1, Read_Data2 ;
  
  reg [31:0] rw_reg [31:0] ;

  integer i ;


  always@( posedge clk or posedge rst ) begin
    if ( rst )
      for ( i = 0 ; i < REGSize ; i = i + 1 ) 
        rw_reg[i] <= 0 ; 
    else 
      if ( Write ) 
        rw_reg[Write_ADDR] <= Write_Data ;
      else if ( Read ) begin
        Read_Data1 <= rw_reg[Read_ADDR1] ;
        Read_Data2 <= rw_reg[Read_ADDR2] ;
      end 
  end  

endmodule