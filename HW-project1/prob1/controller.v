

module controller ( enable_execute, enable_fetch, enable_writeback, sub_opcode, 
                    mux4to1_select, mux2to1_select, imm_reg_select, clk, rst, ir) ;

    input   clk, rst ;
    input   [ 31 : 0 ] ir ;

    output  enable_execute, enable_writeback, enable_fetch, mux2to1_select, imm_reg_select ;
    output  [ 4 : 0 ] sub_opcode ;
    output  [ 1 : 0 ] mux4to1_select ;
    

    reg     enable_execute, enable_writeback, enable_fetch, mux2to1_select, imm_reg_select ;
    reg     [ 1 : 0 ] mux4to1_select, current_state, next_state ;
    reg     [ 4 : 0 ] sub_opcode ;

    wire    [ 5 : 0 ] opcode = ir[ 30 : 25 ] ;
    

    parameter S0 = 2'b00, S1 = 2'b01, S2 = 2'b10, S3 = 2'b11 ;

    always@( posedge clk ) begin 
      if ( rst )  current_state <= S0 ;
      else current_state <= next_state ;
    end

    always@( ir or current_state or opcode) begin
      case ( current_state ) 
        S0 : begin
          next_state = S1 ;
          enable_fetch = 1'b0 ;
          enable_execute = 1'b0 ;
          enable_writeback = 1'b0 ;
          mux2to1_select = 1'b0 ;
          imm_reg_select = 1'b0 ;
        end

        S1 : begin
          next_state = S2 ;
          enable_fetch = 1'b1 ;
          enable_execute = 1'b0 ;
          enable_writeback = 1'b0 ;
          case ( opcode )
            6'b100000 : begin  // R-Type  
              mux2to1_select = 1'b1 ;
              sub_opcode = ir[ 4 : 0 ] ;
              if ( ir[ 24: 10 ] == 15'b0 )
                next_state = S0 ;
              else if ( sub_opcode[4:3] == 2'b00 ) begin  // use reg
                imm_reg_select = 1'b0 ;
              end  
              
              else begin  //  use imm value
                imm_reg_select = 1'b1 ;
                mux4to1_select = 2'b00 ;        
              end  
            end  

            6'b101000 : begin  // ADDI
              imm_reg_select = 1'b1 ;   
              mux4to1_select = 2'b01 ;  // 15bits SE
              mux2to1_select = 1'b1 ;
              sub_opcode = 5'b00000 ;

            end

            6'b101100 : begin  // ORI
              imm_reg_select = 1'b1 ;   
              mux4to1_select = 2'b01 ;  // 15bits ZE
              mux2to1_select = 1'b1 ;
              sub_opcode = 5'b00100 ;
            end

            6'b101011 : begin  // XORI
              imm_reg_select = 1'b1 ;   
              mux4to1_select = 2'b01 ;  // 15bits ZE
              mux2to1_select = 1'b1 ;
              sub_opcode = 5'b00011 ;
            end


            6'b100010 : begin  // MOVI
              imm_reg_select = 1'b1 ;   
              mux4to1_select = 2'b11 ;  // 15bits ZE
              sub_opcode = 5'b01001 ;
              mux2to1_select = 1'b0 ;
            end


            6'b100110 : begin // branch
              imm_reg_select = 1'b1 ;
              mux4to1_select = 2'b01 ;
            end

            default : next_state = S0 ;
          endcase
        end

        S2 : begin
          next_state = S3 ;
          enable_fetch = 1'b0 ;
          enable_execute = 1'b1 ;
          enable_writeback = 1'b0 ;
        end

         S3 : begin
          next_state = S0 ;
          enable_fetch = 1'b0 ;
          enable_execute = 1'b0 ;
          enable_writeback = 1'b1 ;
        end

        default : $display( "Invalid state" ) ;
      endcase    
    end
endmodule
