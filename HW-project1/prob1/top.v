`include "ALU.v"
`include "controller.v"
`include "Mux.v"
`include "RegFile.v"

module top ( clk, rst, instruction, alu_overflow ) ;
    input clk, rst ;
    input [ 31 : 0 ]  instruction ;
    output alu_overflow ;

    wire enable_execute, enable_fetch, enable_writeback, mux2to1_select, imm_reg_select ;
    wire [ 1 : 0 ] mux4to1_select ;
    wire [ 4 : 0 ] sub_opcode, writeback_addr  ;
    wire [ 31 : 0 ] data1, data2, writeback_data, ExToAlu, RfToAlu, alu_result ;

    controller ctrl_unit ( .enable_execute( enable_execute ), .enable_fetch( enable_fetch ), .enable_writeback( enable_writeback ), 
                           .sub_opcode( sub_opcode ), .mux4to1_select( mux4to1_select), .mux2to1_select( mux2to1_select ), 
                           .imm_reg_select( imm_reg_select ), .clk( clk ), .rst( rst ), .ir( instruction ) ) ;

    
    ALU alu ( .enable_execute( enable_execute ), .OverFlow( alu_overflow ), .alu_result( alu_result ), .src1( data1 ), .src2( data2 ), .OP( sub_opcode ) ) ;


    RegFile regfile1 (  .clk( clk ), .rst( rst ), .Write( enable_writeback ), .Read( enable_fetch ), .Write_ADDR( instruction[ 24 : 20 ] ), 
                  .Read_ADDR1( instruction[ 19 : 15 ] ), .Read_ADDR2( instruction[ 14 : 10 ] ), .Write_Data( writeback_data ), .Read_Data1( data1 ), 
                  .Read_Data2( RfToAlu ) ) ;

    mux2to1 imm_reg_mux( .select( imm_reg_select ), .Write_Data1( RfToAlu ), .Write_Data2( ExToAlu ), .Output_Data( data2 ) ) ;

    mux2to1 writeback_mux( .select( mux2to1_select ), .Write_Data1( data2 ), .Write_Data2( alu_result ), .Output_Data( writeback_data ) ) ;

    mux4to1 extension_mux( .select( mux4to1_select ), .Input_data( instruction[ 19 : 0 ] ), .Output_Data( ExToAlu )  ) ;

endmodule
