`timescale 1ns/10ps
`include "top.v"
module top_tb;
  reg clk,rst;  
  reg [31:0] instruction;
  wire alu_overflow ;
  integer i;

  top TOP(.clk(clk), .rst(rst), .instruction(instruction), .alu_overflow(alu_overflow) );
  
  //clock gen.
  always #5 clk=~clk;
  
  initial begin
  clk=0;
  rst=1'b1;
  
  #6  rst=1'b0;
  #4  rst=1'b0; 

      instruction = 32'b0_100010_00010_0000_0000_0000_0000_0111 ;
  #40 instruction = 32'b0_101000_00000_00000_0000_0000_0001_000 ;
  #40 instruction = 32'b0_101000_00001_00001_0000_0000_0000_010 ;
  #40 instruction = 32'b0_100000_00011_00000_00001_00000_00001 ;
  #40 instruction = 32'b0_100000_00100_00001_00010_00000_00000 ;
  #40 instruction = 32'b0_100000_00101_00001_00010_00000_00010 ;
  #40 instruction = 32'b0_100000_00110_00010_00100_00000_00100 ;
  #40 instruction = 32'b0_100000_00111_00011_00101_00000_00011 ;
  #40 instruction = 32'b0_100000_01000_00100_00001_00000_01000 ;
  #40 instruction = 32'b0_100000_01001_00001_00010_00000_01011 ;
  #40 instruction = 32'b0_101100_01010_00000_0000_0000_0011_111 ;
  #40 instruction = 32'b0_101011_01011_00001_0000_0000_0001_010 ;
  #40 instruction = 32'b0_100000_01100_00011_00001_00000_01001 ; 


/*
      instruction = 32'b0_100010_00010_0000_0000_0000_0000_0111 ;  // MOVI R2 = 5'b00111   7
  #40 instruction = 32'b0_100010_00000_0000_0000_0000_0000_0001 ;  // MOVI R0 = 5'b00001   1
  #40 instruction = 32'b0_101000_00000_00000_0000_0000_0000_001 ;  // ADDI R0 = R0 + 5'b00001  2
  #40 instruction = 32'b0_100000_00001_00010_00000_00000_00000 ;   // ADD  R1 = R2 + R0  7 + 2
  #40 instruction = 32'b0_100000_00011_00001_00000_00000_00001 ;   // SUB  R3 = R1 - R0  9 - 2
  #40 instruction = 32'b0_100010_00100_0000_0000_0000_0000_0111 ;  // MOVI R4 = 5'b00111 7
  #40 instruction = 32'b0_100000_00101_00100_00000_00000_00010 ;   // AND  R5 = R4 & R0  7 & 2 = 2
  #40 instruction = 32'b0_100000_00110_00100_00000_00000_00100 ;   // OR  R6 = R4 | R0  7 | 2 = 7
  #40 instruction = 32'b0_100000_00111_00100_00000_00000_00011 ;   // XOR  R7 = R4 ^ R0  7 ^ 2 = 5
  #40 instruction = 32'b0_100000_01000_00100_00010_00000_01001 ;   // SRI  R8 = R4 >> 2  7 >> 2 = 1
  #40 instruction = 32'b0_100000_01001_00100_00010_00000_01000 ;   // SLI  R9 = R4 << 2  7 << 2 = 28
  #40 instruction = 32'b0_100000_01010_00100_11111_00000_01011 ;   // ROTRI  R10 = R4 rot 2  7 rot 2 = 5
*////
      //Design your own test pattern 
      //Note that the time you send a new instruction depends on when the previous instruction is carried out.
  //    instruction = 32'b0_101000_00000_00000_0000_0000_0001_011; //ADDI    R0 = R0 + 5'b01011
  //#30 instruction = 32'b0_101000_00001_00001_0000_0000_0011_111; //ADDI    R1 = R1 + 5'b11111
  
  #40 $display( "done" );
  //test & debug block
  //display register file contents to verify the results
  for( i=0;i<32;i=i+1 ) $display( "register[%d]=%d",i,TOP.regfile1.rw_reg[i] ); 
  #100 $finish;
  end
  initial begin
     $dumpfile( "top.vcd" ) ;
     $dumpvars; 
  end
  
endmodule
  
  
