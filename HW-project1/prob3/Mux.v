module mux2to1( select, Write_Data1, Write_Data2, Output_Data ) ;
  parameter DSize = 32 ;

  input select ;
  input [ DSize -1 : 0 ] Write_Data1, Write_Data2 ;
  output[ DSize - 1 : 0 ] Output_Data ;

  reg [ DSize - 1 : 0 ] Output_Data ;

  always@( select or Write_Data1 or Write_Data2 ) begin
    if ( !select )
      Output_Data = Write_Data1 ;
    else 
      Output_Data = Write_Data2 ;  
  end

endmodule

module mux3to1 ( select, src1, src2, src3, Output_Data ) ;

  input [ 1 : 0 ] select ;
  input [ 31 : 0 ]  src1, src2, src3 ;
  output [ 31 : 0 ] Output_Data ;

  reg   [ 31 : 0 ] Output_Data ;
   
  always@( select or src1 or src2 or src3 ) begin 
    case ( select ) 
      2'b00 : Output_Data = src1 ;
      2'b01 : Output_Data = src2 ;
      2'b10 : Output_Data = src3 ;  
    endcase
  end

endmodule


module Sign_Extend_Mux( select, Input_data, Output_Data ) ;
  parameter DSize = 32 ;
  
  input [ 1 : 0 ] select ;
  input [ 19 : 0 ] Input_data ;
  output [ 31 : 0 ] Output_Data ;

  reg [ 31 : 0 ] Output_Data ;


  always@( select or Input_data ) begin
    case( select ) 
      2'b00 : begin
        Output_Data = { 27'd0, Input_data[ 14 : 10 ] } ;
      end

      2'b01 : begin
        Output_Data = { 17'd0, Input_data[ 14 : 0 ] } ;
      end 

      2'b10 : begin
        Output_Data = { 17'd0, Input_data[ 14 : 10 ] } ;
      end 

      2'b11 : begin
        Output_Data = { 12'd0, Input_data[ 19 : 0 ] } ;
      end
    endcase  
  end

endmodule