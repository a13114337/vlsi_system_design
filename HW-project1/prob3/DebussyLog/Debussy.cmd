srcSourceCodeView
srcResizeWindow 2 26 804 500
wvCreateWindow
wvResizeWindow -win $_nWave2 50 214 960 332
wvResizeWindow -win $_nWave2 52 237 960 332
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvResizeWindow -win $_nWave2 -4 26 1920 1007
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 1)}
wvSetPosition -win $_nWave2 {("G1" 1)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSetPosition -win $_nWave2 {("G1" 1)}
wvSetPosition -win $_nWave2 {("G1" 5)}
wvSetPosition -win $_nWave2 {("G1" 5)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 2 3 4 5 )} 
wvSetPosition -win $_nWave2 {("G1" 5)}
wvSetCursor -win $_nWave2 190588.129773 -snap {("G2" 0)}
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSetCursor -win $_nWave2 57891.066365 -snap {("G1" 4)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvSetPosition -win $_nWave2 {("G1" 6)}
wvSetPosition -win $_nWave2 {("G1" 6)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 6 )} 
wvSetPosition -win $_nWave2 {("G1" 6)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/WB_mux"
wvSetPosition -win $_nWave2 {("G1" 11)}
wvSetPosition -win $_nWave2 {("G1" 11)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 7 8 9 10 11 )} 
wvSetPosition -win $_nWave2 {("G1" 11)}
wvSetPosition -win $_nWave2 {("G1" 11)}
wvSetPosition -win $_nWave2 {("G1" 11)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 7 8 9 10 11 )} 
wvSetPosition -win $_nWave2 {("G1" 11)}
wvGetSignalClose -win $_nWave2
wvSetCursor -win $_nWave2 56906.425069 -snap {("G1" 10)}
wvSetCursor -win $_nWave2 58119.214958 -snap {("G1" 8)}
wvSetCursor -win $_nWave2 57494.808283 -snap {("G1" 10)}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/WB_mux"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 12)}
wvSetPosition -win $_nWave2 {("G1" 12)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/TOP/instruction\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 12 )} 
wvSetPosition -win $_nWave2 {("G1" 12)}
wvSetCursor -win $_nWave2 62081.795781 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 12 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSelectSignal -win $_nWave2 {( "G1" 12 )} 
wvSetRadix -win $_nWave2 -format Bin
wvSetCursor -win $_nWave2 62430.022580 -snap {("G1" 10)}
wvSetCursor -win $_nWave2 62406.006939 -snap {("G1" 10)}
wvSetCursor -win $_nWave2 62478.053863 -snap {("G1" 12)}
wvResizeWindow -win $_nWave2 -4 26 1920 1007
wvSetCursor -win $_nWave2 53945.880410 -snap {("G2" 0)}
wvSetCursor -win $_nWave2 4336.343900 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSelectSignal -win $_nWave2 {( "G1" 4 )} 
wvSetCursor -win $_nWave2 9885.903129 -snap {("G1" 4)}
wvSetCursor -win $_nWave2 9933.951261 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 15171.197633 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 14558.583952 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 15387.414227 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 9633.650437 -snap {("G2" 0)}
wvSetPosition -win $_nWave2 {("G1" 48)}
wvSetPosition -win $_nWave2 {("G1" 48)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 13 14 15 16 17 18 19 20 21 22 23 24 25 26 \
           27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 \
           )} 
wvSetPosition -win $_nWave2 {("G1" 48)}
wvSetPosition -win $_nWave2 {("G1" 48)}
wvSetPosition -win $_nWave2 {("G1" 48)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 13 14 15 16 17 18 19 20 21 22 23 24 25 26 \
           27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 \
           )} 
wvSetPosition -win $_nWave2 {("G1" 48)}
wvGetSignalClose -win $_nWave2
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvSelectSignal -win $_nWave2 {( "G1" 36 )} 
wvSelectSignal -win $_nWave2 {( "G1" 38 )} 
wvSetPosition -win $_nWave2 {("G1" 38)}
wvSelectSignal -win $_nWave2 {( "G1" 35 )} 
wvSelectSignal -win $_nWave2 {( "G1" 34 )} 
wvSetPosition -win $_nWave2 {("G1" 34)}
wvSetPosition -win $_nWave2 {("G1" 37)}
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 37)}
wvSelectSignal -win $_nWave2 {( "G1" 34 )} 
wvScrollUp -win $_nWave2 1
wvZoomOut -win $_nWave2
wvSelectSignal -win $_nWave2 {( "G1" 6 )} 
wvSelectSignal -win $_nWave2 {( "G1" 30 )} 
wvScrollDown -win $_nWave2 1
wvSelectSignal -win $_nWave2 {( "G1" 42 )} 
wvSelectSignal -win $_nWave2 {( "G1" 41 )} 
wvSelectSignal -win $_nWave2 {( "G1" 19 )} 
wvScrollDown -win $_nWave2 0
wvSetCursor -win $_nWave2 3579.585823 -snap {("G1" 22)}
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvCloseFile -win $_nWave2
wvDisplayGridCount -win $_nWave2 -off
wvGetSignalClose -win $_nWave2
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 36)}
wvSetPosition -win $_nWave2 {("G1" 36)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 \
           18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 )} 
wvSetPosition -win $_nWave2 {("G1" 36)}
wvSetPosition -win $_nWave2 {("G1" 36)}
wvSetPosition -win $_nWave2 {("G1" 36)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 \
           18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 )} 
wvSetPosition -win $_nWave2 {("G1" 36)}
wvGetSignalClose -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvSelectSignal -win $_nWave2 {( "G1" 18 )} 
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvSetPosition -win $_nWave2 {("G1" 46)}
wvSetPosition -win $_nWave2 {("G1" 46)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 37 38 39 40 41 42 43 44 45 46 )} 
wvSetPosition -win $_nWave2 {("G1" 46)}
wvSetPosition -win $_nWave2 {("G1" 46)}
wvSetPosition -win $_nWave2 {("G1" 46)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 37 38 39 40 41 42 43 44 45 46 )} 
wvSetPosition -win $_nWave2 {("G1" 46)}
wvGetSignalClose -win $_nWave2
wvSetCursor -win $_nWave2 3824.745357 -snap {("G1" 41)}
wvSelectSignal -win $_nWave2 {( "G1" 39 )} 
wvSetCursor -win $_nWave2 8256.950270 -snap {("G1" 39)}
wvSetCursor -win $_nWave2 3397.273817 -snap {("G1" 46)}
wvSetCursor -win $_nWave2 3982.234871 -snap {("G1" 45)}
wvSetCursor -win $_nWave2 5782.115039 -snap {("G1" 36)}
wvSelectSignal -win $_nWave2 {( "G1" 25 )} 
wvSetPosition -win $_nWave2 {("G1" 25)}
wvSelectSignal -win $_nWave2 {( "G1" 24 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 25)}
wvSelectSignal -win $_nWave2 {( "G1" 22 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 25)}
wvSetCursor -win $_nWave2 2474.835231 -snap {("G1" 25)}
wvSelectSignal -win $_nWave2 {( "G1" 24 )} 
wvSelectSignal -win $_nWave2 {( "G1" 23 )} 
wvSetCursor -win $_nWave2 2429.838226 -snap {("G1" 31)}
wvSetCursor -win $_nWave2 1979.868185 -snap {("G1" 36)}
wvSetCursor -win $_nWave2 1777.381666 -snap {("G1" 43)}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave2 {("G1" 26)}
wvSetPosition -win $_nWave2 {("G1" 26)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvSetPosition -win $_nWave2 {("G1" 26)}
wvSetPosition -win $_nWave2 {("G1" 27)}
wvSetPosition -win $_nWave2 {("G1" 27)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 27 )} 
wvSetPosition -win $_nWave2 {("G1" 27)}
wvSetPosition -win $_nWave2 {("G1" 27)}
wvSetPosition -win $_nWave2 {("G1" 27)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 27 )} 
wvSetPosition -win $_nWave2 {("G1" 27)}
wvGetSignalClose -win $_nWave2
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/WB_mux"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave2 {("G1" 43)}
wvSetPosition -win $_nWave2 {("G1" 43)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/regfile1/ADSize} \
{/top_tb/TOP/regfile1/DASize} \
{/top_tb/TOP/regfile1/REGSize} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_ADDR2\[4:0\]} \
{/top_tb/TOP/regfile1/Read_ADDR3\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/regfile1/Read_Data2\[31:0\]} \
{/top_tb/TOP/regfile1/Read_Data3\[31:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/clk} \
{/top_tb/TOP/regfile1/i} \
{/top_tb/TOP/regfile1/rst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 28 29 30 31 32 33 34 35 36 37 38 39 40 41 \
           42 43 )} 
wvSetPosition -win $_nWave2 {("G1" 43)}
wvSetPosition -win $_nWave2 {("G1" 43)}
wvSetPosition -win $_nWave2 {("G1" 43)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/regfile1/ADSize} \
{/top_tb/TOP/regfile1/DASize} \
{/top_tb/TOP/regfile1/REGSize} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_ADDR2\[4:0\]} \
{/top_tb/TOP/regfile1/Read_ADDR3\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/regfile1/Read_Data2\[31:0\]} \
{/top_tb/TOP/regfile1/Read_Data3\[31:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/clk} \
{/top_tb/TOP/regfile1/i} \
{/top_tb/TOP/regfile1/rst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 28 29 30 31 32 33 34 35 36 37 38 39 40 41 \
           42 43 )} 
wvSetPosition -win $_nWave2 {("G1" 43)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/IM1"
wvSetPosition -win $_nWave2 {("G1" 54)}
wvSetPosition -win $_nWave2 {("G1" 54)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/regfile1/ADSize} \
{/top_tb/TOP/regfile1/DASize} \
{/top_tb/TOP/regfile1/REGSize} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/TOP/regfile1/Read_ADDR1\[4:0\]} \
{/top_tb/TOP/regfile1/Read_ADDR2\[4:0\]} \
{/top_tb/TOP/regfile1/Read_ADDR3\[4:0\]} \
{/top_tb/TOP/regfile1/Read_Data1\[31:0\]} \
{/top_tb/TOP/regfile1/Read_Data2\[31:0\]} \
{/top_tb/TOP/regfile1/Read_Data3\[31:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/clk} \
{/top_tb/TOP/regfile1/i} \
{/top_tb/TOP/regfile1/rst} \
{/top_tb/IM1/IM_address\[9:0\]} \
{/top_tb/IM1/IMin\[31:0\]} \
{/top_tb/IM1/IMout\[31:0\]} \
{/top_tb/IM1/clk} \
{/top_tb/IM1/data_size} \
{/top_tb/IM1/enable_fetch} \
{/top_tb/IM1/enable_mem} \
{/top_tb/IM1/enable_write} \
{/top_tb/IM1/i} \
{/top_tb/IM1/mem_size} \
{/top_tb/IM1/rst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/alu/ISize} \
{/top_tb/TOP/alu/OPSize} \
{/top_tb/TOP/alu/OP\[4:0\]} \
{/top_tb/TOP/alu/OverFlow} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/TOP/alu/enable_execute} \
{/top_tb/TOP/alu/i} \
{/top_tb/TOP/alu/src1\[31:0\]} \
{/top_tb/TOP/alu/src2\[31:0\]} \
{/top_tb/TOP/alu/temp\[63:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 44 45 46 47 48 49 50 51 52 53 54 )} 
wvSetPosition -win $_nWave2 {("G1" 54)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSelectSignal -win $_nWave2 {( "G1" 37 )} 
wvScrollUp -win $_nWave2 15
wvScrollDown -win $_nWave2 15
wvSelectSignal -win $_nWave2 {( "G1" 60 )} 
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 2
wvScrollDown -win $_nWave2 7
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvSetPosition -win $_nWave2 {("G1" 26)}
wvSelectSignal -win $_nWave2 {( "G1" 25 )} 
wvSetPosition -win $_nWave2 {("G1" 25)}
wvScrollDown -win $_nWave2 11
wvSelectSignal -win $_nWave2 {( "G1" 56 )} 
wvScrollDown -win $_nWave2 8
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 25)}
wvSetPosition -win $_nWave2 {("G1" 26)}
wvSelectSignal -win $_nWave2 {( "G1" 57 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 26)}
wvSetPosition -win $_nWave2 {("G1" 27)}
wvScrollUp -win $_nWave2 20
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvSetCursor -win $_nWave2 4657.189934 -snap {("G1" 63)}
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvSetCursor -win $_nWave2 8009.466747 -snap {("G1" 63)}
wvSetCursor -win $_nWave2 13431.605752 -snap {("G1" 63)}
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvSetCursor -win $_nWave2 5939.604554 -snap {("G1" 68)}
wvSetCursor -win $_nWave2 19416.207310 -snap {("G1" 71)}
wvSetCursor -win $_nWave2 48725.730839 -snap {("G1" 59)}
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvSelectSignal -win $_nWave2 {( "G1" 13 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 27)}
wvSelectSignal -win $_nWave2 {( "G1" 27 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSetCursor -win $_nWave2 53247.929760 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 53540.410288 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 53315.425267 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 56240.230539 -snap {("G1" 26)}
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvSetCursor -win $_nWave2 53270.428262 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 53495.413283 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 53450.416279 -snap {("G1" 25)}
wvSetCursor -win $_nWave2 57432.651150 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 43641.069365 -snap {("G1" 31)}
wvSetCursor -win $_nWave2 36312.582385 -snap {("G1" 29)}
wvSetCursor -win $_nWave2 36245.086878 -snap {("G1" 29)}
wvSetCursor -win $_nWave2 57015.704014 -snap {("G1" 27)}
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvSelectSignal -win $_nWave2 {( "G1" 25 )} 
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvSetCursor -win $_nWave2 51604.814260 -snap {("G1" 25)}
wvSetCursor -win $_nWave2 55415.227097 -snap {("G1" 26)}
wvSetCursor -win $_nWave2 41106.179763 -snap {("G1" 42)}
wvSetCursor -win $_nWave2 44480.955078 -snap {("G1" 28)}
wvSetCursor -win $_nWave2 55617.713616 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 40588.714215 -snap {("G1" 22)}
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollUp -win $_nWave2 1
wvScrollDown -win $_nWave2 0
wvScrollDown -win $_nWave2 0
wvSetCursor -win $_nWave2 55595.215113 -snap {("G1" 27)}
wvSetCursor -win $_nWave2 42613.579403 -snap {("G1" 40)}
wvCloseFile -win $_nWave2
wvDisplayGridCount -win $_nWave2 -off
wvGetSignalClose -win $_nWave2
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 36)}
wvSetPosition -win $_nWave2 {("G1" 36)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 \
           18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 )} 
wvSetPosition -win $_nWave2 {("G1" 36)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/pc_adder"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/ctrl_unit"
wvSetPosition -win $_nWave2 {("G1" 59)}
wvSetPosition -win $_nWave2 {("G1" 59)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/DM_address\[11:0\]} \
{/top_tb/TOP/DM_enable} \
{/top_tb/TOP/DM_in\[31:0\]} \
{/top_tb/TOP/DM_out\[31:0\]} \
{/top_tb/TOP/DM_read} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/ExToAlu\[31:0\]} \
{/top_tb/TOP/IM_address\[9:0\]} \
{/top_tb/TOP/IM_enable} \
{/top_tb/TOP/IM_read} \
{/top_tb/TOP/IM_write} \
{/top_tb/TOP/MuxToShift\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/PC_next\[9:0\]} \
{/top_tb/TOP/RfToAlu\[31:0\]} \
{/top_tb/TOP/SV\[1:0\]} \
{/top_tb/TOP/alu_overflow} \
{/top_tb/TOP/alu_result\[31:0\]} \
{/top_tb/TOP/clk} \
{/top_tb/TOP/data1\[31:0\]} \
{/top_tb/TOP/data2\[31:0\]} \
{/top_tb/TOP/enable_execute} \
{/top_tb/TOP/enable_fetch_DM} \
{/top_tb/TOP/enable_fetch_decode} \
{/top_tb/TOP/enable_fetch_inst} \
{/top_tb/TOP/enable_shift} \
{/top_tb/TOP/enable_write_DM} \
{/top_tb/TOP/enable_write_RF} \
{/top_tb/TOP/extend_mux_select\[1:0\]} \
{/top_tb/TOP/imm_reg_select} \
{/top_tb/TOP/instruction\[31:0\]} \
{/top_tb/TOP/rst} \
{/top_tb/TOP/sub_opcode\[4:0\]} \
{/top_tb/TOP/write_data_Mux_RF\[31:0\]} \
{/top_tb/TOP/write_data_RF_DM\[31:0\]} \
{/top_tb/TOP/writeback_mux_select\[1:0\]} \
{/top_tb/TOP/ctrl_unit/S0} \
{/top_tb/TOP/ctrl_unit/S1} \
{/top_tb/TOP/ctrl_unit/S2} \
{/top_tb/TOP/ctrl_unit/S3} \
{/top_tb/TOP/ctrl_unit/SV\[1:0\]} \
{/top_tb/TOP/ctrl_unit/clk} \
{/top_tb/TOP/ctrl_unit/current_state\[1:0\]} \
{/top_tb/TOP/ctrl_unit/enable_execute} \
{/top_tb/TOP/ctrl_unit/enable_fetch_decode} \
{/top_tb/TOP/ctrl_unit/enable_fetch_inst} \
{/top_tb/TOP/ctrl_unit/enable_read_DM} \
{/top_tb/TOP/ctrl_unit/enable_shift} \
{/top_tb/TOP/ctrl_unit/enable_write_DM} \
{/top_tb/TOP/ctrl_unit/enable_write_DM_reg} \
{/top_tb/TOP/ctrl_unit/enable_write_RF} \
{/top_tb/TOP/ctrl_unit/extend_mux_select\[1:0\]} \
{/top_tb/TOP/ctrl_unit/imm_reg_select} \
{/top_tb/TOP/ctrl_unit/ir\[31:0\]} \
{/top_tb/TOP/ctrl_unit/next_state\[1:0\]} \
{/top_tb/TOP/ctrl_unit/opcode\[5:0\]} \
{/top_tb/TOP/ctrl_unit/rst} \
{/top_tb/TOP/ctrl_unit/sub_opcode\[4:0\]} \
{/top_tb/TOP/ctrl_unit/writeback_select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 37 38 39 40 41 42 43 44 45 46 47 48 49 50 \
           51 52 53 54 55 56 57 58 59 )} 
wvSetPosition -win $_nWave2 {("G1" 59)}
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvSelectSignal -win $_nWave2 {( "G1" 25 )} 
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvSetPosition -win $_nWave2 {("G1" 26)}
wvSelectSignal -win $_nWave2 {( "G1" 23 )} 
wvSelectSignal -win $_nWave2 {( "G1" 26 )} 
wvScrollDown -win $_nWave2 0
wvSelectSignal -win $_nWave2 {( "G1" 46 )} 
wvSelectSignal -win $_nWave2 {( "G1" 47 )} 
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 44 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 44 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 44 )} 
wvSelectSignal -win $_nWave2 {( "G1" 45 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 45 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 46 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 49 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 47)}
wvSetPosition -win $_nWave2 {("G1" 48)}
wvSelectSignal -win $_nWave2 {( "G1" 47 )} 
wvSetCursor -win $_nWave2 28121.030557 -snap {("G1" 48)}
wvSelectSignal -win $_nWave2 {( "G1" 13 )} 
wvMoveSelected -win $_nWave2
wvSetPosition -win $_nWave2 {("G1" 48)}
wvSelectSignal -win $_nWave2 {( "G1" 48 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSetCursor -win $_nWave2 56353.505093 -snap {("G1" 48)}
wvSelectSignal -win $_nWave2 {( "G1" 46 )} 
wvSetCursor -win $_nWave2 48923.906531 -snap {("G1" 52)}
wvSetCursor -win $_nWave2 52638.705812 -snap {("G1" 47)}
wvSetCursor -win $_nWave2 54718.993409 -snap {("G1" 47)}
wvSetCursor -win $_nWave2 55870.581186 -snap {("G1" 48)}
wvSelectSignal -win $_nWave2 {( "G1" 47 )} 
wvSetCursor -win $_nWave2 53121.629718 -snap {("G1" 47)}
wvSetCursor -win $_nWave2 57245.056920 -snap {("G1" 47)}
wvSetCursor -win $_nWave2 55870.581186 -snap {("G1" 47)}
wvSetCursor -win $_nWave2 59139.604554 -snap {("G1" 46)}
wvSetCursor -win $_nWave2 59028.160575 -snap {("G1" 47)}
wvSetCursor -win $_nWave2 59028.160575 -snap {("G1" 47)}
wvSelectSignal -win $_nWave2 {( "G1" 50 )} 
wvSetCursor -win $_nWave2 48292.390653 -snap {("G1" 46)}
wvCloseFile -win $_nWave2
wvDisplayGridCount -win $_nWave2 -off
wvGetSignalClose -win $_nWave2
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvSetPosition -win $_nWave2 {("G1" 1)}
wvSetPosition -win $_nWave2 {("G1" 1)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DMout\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSetPosition -win $_nWave2 {("G1" 1)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 3)}
wvSetPosition -win $_nWave2 {("G1" 3)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DMout\[31:0\]} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 2 3 )} 
wvSetPosition -win $_nWave2 {("G1" 3)}
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSetRadix -win $_nWave2 -format UDec
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomOut -win $_nWave2
wvSetCursor -win $_nWave2 58359.496705 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 60774.116237 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 58842.420611 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 60774.116237 -snap {("G1" 3)}
wvSetCursor -win $_nWave2 61034.152187 -snap {("G1" 2)}
wvSetCursor -win $_nWave2 60736.968244 -snap {("G1" 2)}
wvSelectSignal -win $_nWave2 {( "G1" 2 )} 
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvSetPosition -win $_nWave2 {("G1" 6)}
wvSetPosition -win $_nWave2 {("G1" 6)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DMout\[31:0\]} \
{/top_tb/TOP/DM_write} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 4 5 6 )} 
wvSetPosition -win $_nWave2 {("G1" 6)}
wvSetCursor -win $_nWave2 15750.748951 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 2 )} 
wvCut -win $_nWave2
wvSetPosition -win $_nWave2 {("G2" 0)}
wvSetPosition -win $_nWave2 {("G1" 5)}
wvSelectSignal -win $_nWave2 {( "G1" 5 )} 
wvSetPosition -win $_nWave2 {("G1" 7)}
wvSetPosition -win $_nWave2 {("G1" 7)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DMout\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 6 7 )} 
wvSetPosition -win $_nWave2 {("G1" 7)}
wvSetPosition -win $_nWave2 {("G1" 7)}
wvSetPosition -win $_nWave2 {("G1" 7)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DMout\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 6 7 )} 
wvSetPosition -win $_nWave2 {("G1" 7)}
wvGetSignalClose -win $_nWave2
wvSelectSignal -win $_nWave2 {( "G1" 6 )} 
wvSelectSignal -win $_nWave2 {( "G1" 7 )} 
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvGetSignalClose -win $_nWave2
wvCloseFile -win $_nWave2
wvDisplayGridCount -win $_nWave2 -off
wvGetSignalClose -win $_nWave2
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvSetPosition -win $_nWave2 {("G1" 2)}
wvSetPosition -win $_nWave2 {("G1" 2)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 2 )} 
wvSetPosition -win $_nWave2 {("G1" 2)}
wvSetPosition -win $_nWave2 {("G1" 6)}
wvSetPosition -win $_nWave2 {("G1" 6)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 3 4 5 6 )} 
wvSetPosition -win $_nWave2 {("G1" 6)}
wvSetPosition -win $_nWave2 {("G1" 6)}
wvSetPosition -win $_nWave2 {("G1" 6)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 3 4 5 6 )} 
wvSetPosition -win $_nWave2 {("G1" 6)}
wvGetSignalClose -win $_nWave2
wvZoomOut -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvSetCursor -win $_nWave2 57873.483277 -snap {("G1" 2)}
wvSelectSignal -win $_nWave2 {( "G1" 5 )} 
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 7)}
wvSetPosition -win $_nWave2 {("G1" 7)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 7 )} 
wvSetPosition -win $_nWave2 {("G1" 7)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave2 {("G1" 8)}
wvSetPosition -win $_nWave2 {("G1" 8)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 8 )} 
wvSetPosition -win $_nWave2 {("G1" 8)}
wvSetPosition -win $_nWave2 {("G1" 9)}
wvSetPosition -win $_nWave2 {("G1" 9)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 9 )} 
wvSetPosition -win $_nWave2 {("G1" 9)}
wvSetPosition -win $_nWave2 {("G1" 9)}
wvSetPosition -win $_nWave2 {("G1" 9)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 9 )} 
wvSetPosition -win $_nWave2 {("G1" 9)}
wvGetSignalClose -win $_nWave2
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave2 {("G1" 10)}
wvSetPosition -win $_nWave2 {("G1" 10)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 10 )} 
wvSetPosition -win $_nWave2 {("G1" 10)}
wvSetCursor -win $_nWave2 53655.014134 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 7 )} 
wvSelectSignal -win $_nWave2 {( "G1" 7 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSelectSignal -win $_nWave2 {( "G1" 9 )} 
wvSelectSignal -win $_nWave2 {( "G1" 9 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSelectSignal -win $_nWave2 {( "G1" 8 )} 
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/WB_mux"
wvSetPosition -win $_nWave2 {("G1" 11)}
wvSetPosition -win $_nWave2 {("G1" 11)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 11 )} 
wvSetPosition -win $_nWave2 {("G1" 11)}
wvSetPosition -win $_nWave2 {("G1" 12)}
wvSetPosition -win $_nWave2 {("G1" 12)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 12 )} 
wvSetPosition -win $_nWave2 {("G1" 12)}
wvSetPosition -win $_nWave2 {("G1" 13)}
wvSetPosition -win $_nWave2 {("G1" 13)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 13 )} 
wvSetPosition -win $_nWave2 {("G1" 13)}
wvSetPosition -win $_nWave2 {("G1" 14)}
wvSetPosition -win $_nWave2 {("G1" 14)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 14 )} 
wvSetPosition -win $_nWave2 {("G1" 14)}
wvSelectGroup -win $_nWave2 {G2}
wvSetPosition -win $_nWave2 {("G1" 15)}
wvSetPosition -win $_nWave2 {("G1" 15)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 15 )} 
wvSetPosition -win $_nWave2 {("G1" 15)}
wvSetCursor -win $_nWave2 53745.008142 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 11 )} 
wvSetCursor -win $_nWave2 53306.287351 -snap {("G2" 0)}
wvSetCursor -win $_nWave2 56636.065662 -snap {("G1" 15)}
wvSetCursor -win $_nWave2 56411.080641 -snap {("G1" 15)}
wvSetCursor -win $_nWave2 54926.179502 -snap {("G1" 15)}
wvSetCursor -win $_nWave2 53778.755895 -snap {("G1" 15)}
wvSetCursor -win $_nWave2 55567.386812 -snap {("G1" 15)}
wvSetCursor -win $_nWave2 56186.095620 -snap {("G1" 12)}
wvSelectGroup -win $_nWave2 {G2}
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSetCursor -win $_nWave2 56759.807423 -snap {("G1" 2)}
wvCloseFile -win $_nWave2
wvDisplayGridCount -win $_nWave2 -off
wvGetSignalClose -win $_nWave2
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/WB_mux"
wvSetPosition -win $_nWave2 {("G1" 5)}
wvSetPosition -win $_nWave2 {("G1" 5)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 2 3 4 5 )} 
wvSetPosition -win $_nWave2 {("G1" 5)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvSetPosition -win $_nWave2 {("G1" 10)}
wvSetPosition -win $_nWave2 {("G1" 10)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 6 7 8 9 10 )} 
wvSetPosition -win $_nWave2 {("G1" 10)}
wvZoomOut -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvSelectSignal -win $_nWave2 {( "G1" 10 )} 
wvSelectSignal -win $_nWave2 {( "G1" 9 )} 
wvSelectSignal -win $_nWave2 {( "G1" 8 )} 
wvSelectSignal -win $_nWave2 {( "G1" 9 )} 
wvGetSignalClose -win $_nWave2
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/ctrl_unit"
wvSetPosition -win $_nWave2 {("G1" 13)}
wvSetPosition -win $_nWave2 {("G1" 13)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/TOP/WB_mux/Output_Data\[31:0\]} \
{/top_tb/TOP/WB_mux/select\[1:0\]} \
{/top_tb/TOP/WB_mux/src1\[31:0\]} \
{/top_tb/TOP/WB_mux/src2\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/TOP/ctrl_unit/enable_write_DM} \
{/top_tb/TOP/ctrl_unit/enable_write_DM_reg} \
{/top_tb/TOP/ctrl_unit/enable_write_RF} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 11 12 13 )} 
wvSetPosition -win $_nWave2 {("G1" 13)}
wvSelectSignal -win $_nWave2 {( "G1" 11 )} 
wvResizeWindow -win $_nWave2 -4 26 1920 1007
wvCloseFile -win $_nWave2
wvDisplayGridCount -win $_nWave2 -off
wvGetSignalClose -win $_nWave2
wvConvertFile -win $_nWave2 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd"
wvSetPosition -win $_nWave2 {("G1" 0)}
wvOpenFile -win $_nWave2 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob3/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave2
wvGetSignalSetScope -win $_nWave2 "/top_tb"
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvSetPosition -win $_nWave2 {("G1" 1)}
wvSetPosition -win $_nWave2 {("G1" 1)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSetPosition -win $_nWave2 {("G1" 1)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP"
wvSetPosition -win $_nWave2 {("G1" 2)}
wvSetPosition -win $_nWave2 {("G1" 2)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 2 )} 
wvSetPosition -win $_nWave2 {("G1" 2)}
wvGetSignalSetScope -win $_nWave2 "/top_tb/TOP/alu"
wvSetPosition -win $_nWave2 {("G1" 3)}
wvSetPosition -win $_nWave2 {("G1" 3)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSetPosition -win $_nWave2 {("G1" 3)}
wvSetCursor -win $_nWave2 22852.515337 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 2 )} 
wvSelectSignal -win $_nWave2 {( "G1" 2 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSetCursor -win $_nWave2 60540.736196 -snap {("G1" 2)}
wvSetCursor -win $_nWave2 61462.208589 -snap {("G1" 2)}
wvZoomOut -win $_nWave2
wvZoomIn -win $_nWave2
wvZoomIn -win $_nWave2
wvSetCursor -win $_nWave2 60597.871771 -snap {("G1" 2)}
wvSetCursor -win $_nWave2 60183.209194 -snap {("G1" 2)}
wvSetCursor -win $_nWave2 61703.638642 -snap {("G1" 2)}
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSelectSignal -win $_nWave2 {( "G1" 3 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSetRadix -win $_nWave2 -format UDec
wvGetSignalSetScope -win $_nWave2 "/top_tb/DM1"
wvSetPosition -win $_nWave2 {("G1" 14)}
wvSetPosition -win $_nWave2 {("G1" 14)}
wvAddSignal -win $_nWave2 -clear
wvAddSignal -win $_nWave2 -group {"G1" \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/alu/alu_result\[31:0\]} \
{/top_tb/DM1/DM_address\[11:0\]} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/clk} \
{/top_tb/DM1/data_size} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_mem} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/DM1/i} \
{/top_tb/DM1/mem_size} \
{/top_tb/DM1/rst} \
}
wvAddSignal -win $_nWave2 -group {"G2" \
}
wvSelectSignal -win $_nWave2 {( "G1" 4 5 6 7 8 9 10 11 12 13 14 )} 
wvSetPosition -win $_nWave2 {("G1" 14)}
wvSetCursor -win $_nWave2 41938.055820 -snap {("G2" 0)}
wvSelectSignal -win $_nWave2 {( "G1" 11 )} 
wvSetCursor -win $_nWave2 61012.534347 -snap {("G1" 11)}
wvSetCursor -win $_nWave2 62901.552752 -snap {("G1" 11)}
wvSetCursor -win $_nWave2 64099.466863 -snap {("G1" 10)}
wvSetCursor -win $_nWave2 60654.369420 -snap {("G1" 11)}
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSelectSignal -win $_nWave2 {( "G1" 1 )} 
wvSetRadix -win $_nWave2 -format UDec
wvSetCursor -win $_nWave2 54895.166966 -snap {("G2" 0)}
wvGetSignalClose -win $_nWave2
wvCloseWindow -win $_nWave2
wvCreateWindow
wvResizeWindow -win $_nWave3 50 214 960 332
wvResizeWindow -win $_nWave3 52 237 960 332
wvConvertFile -win $_nWave3 -o \
           "/home/user1/test15/test1506/HW-project1/homework1/prob4-5/top.vcd.fsdb" \
           "/home/user1/test15/test1506/HW-project1/homework1/prob4-5/top.vcd"
wvOpenFile -win $_nWave3 \
           {/home/user1/test15/test1506/HW-project1/homework1/prob4-5/top.vcd.fsdb}
wvGetSignalOpen -win $_nWave3
wvGetSignalSetScope -win $_nWave3 "/top_tb"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/reg_PC"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave3 {("G1" 2)}
wvSetPosition -win $_nWave3 {("G1" 2)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 1 2 )} 
wvSetPosition -win $_nWave3 {("G1" 2)}
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/WB_mux"
wvSetPosition -win $_nWave3 {("G1" 2)}
wvSetPosition -win $_nWave3 {("G1" 2)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 1 2 )} 
wvSetPosition -win $_nWave3 {("G1" 2)}
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/reg_PC"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/pc_adder"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvSetPosition -win $_nWave3 {("G1" 3)}
wvSetPosition -win $_nWave3 {("G1" 3)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 3 )} 
wvSetPosition -win $_nWave3 {("G1" 3)}
wvSetPosition -win $_nWave3 {("G1" 3)}
wvSetPosition -win $_nWave3 {("G1" 3)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 3 )} 
wvSetPosition -win $_nWave3 {("G1" 3)}
wvGetSignalClose -win $_nWave3
wvSelectSignal -win $_nWave3 {( "G1" 3 )} 
wvSetRadix -win $_nWave3 -format UDec
wvZoomIn -win $_nWave3
wvZoomIn -win $_nWave3
wvResizeWindow -win $_nWave3 237 279 960 331
wvSetCursor -win $_nWave3 69345.267542 -snap {("G2" 0)}
wvSetCursor -win $_nWave3 68616.612949 -snap {("G2" 0)}
wvSetCursor -win $_nWave3 52829.096778 -snap {("G2" 0)}
wvSetCursor -win $_nWave3 61718.682807 -snap {("G1" 2)}
wvGetSignalOpen -win $_nWave3
wvGetSignalSetScope -win $_nWave3 "/top_tb"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave3 {("G1" 3)}
wvSetPosition -win $_nWave3 {("G1" 3)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSetPosition -win $_nWave3 {("G1" 3)}
wvGetSignalClose -win $_nWave3
wvGetSignalOpen -win $_nWave3
wvGetSignalSetScope -win $_nWave3 "/top_tb"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave3 {("G1" 4)}
wvSetPosition -win $_nWave3 {("G1" 4)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 4 )} 
wvSetPosition -win $_nWave3 {("G1" 4)}
wvSetPosition -win $_nWave3 {("G1" 4)}
wvSetPosition -win $_nWave3 {("G1" 4)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 4 )} 
wvSetPosition -win $_nWave3 {("G1" 4)}
wvGetSignalClose -win $_nWave3
wvZoomIn -win $_nWave3
wvSelectSignal -win $_nWave3 {( "G1" 2 )} 
wvResizeWindow -win $_nWave3 -4 26 1920 1007
wvResizeWindow -win $_nWave3 -4 26 1920 1007
wvSelectSignal -win $_nWave3 {( "G1" 2 )} 
wvSetRadix -win $_nWave3 -format UDec
wvSetCursor -win $_nWave3 54992.121569 -snap {("G1" 2)}
wvSetCursor -win $_nWave3 56675.021068 -snap {("G1" 4)}
wvSetCursor -win $_nWave3 60521.648494 -snap {("G1" 3)}
wvGetSignalOpen -win $_nWave3
wvGetSignalSetScope -win $_nWave3 "/top_tb"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvSetPosition -win $_nWave3 {("G1" 5)}
wvSetPosition -win $_nWave3 {("G1" 5)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Read} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 5 )} 
wvSetPosition -win $_nWave3 {("G1" 5)}
wvSetPosition -win $_nWave3 {("G1" 5)}
wvSetPosition -win $_nWave3 {("G1" 5)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Read} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 5 )} 
wvSetPosition -win $_nWave3 {("G1" 5)}
wvGetSignalClose -win $_nWave3
wvSelectSignal -win $_nWave3 {( "G1" 2 )} 
wvGetSignalOpen -win $_nWave3
wvGetSignalSetScope -win $_nWave3 "/top_tb"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave3 "/top_tb/DM1"
wvSetPosition -win $_nWave3 {("G1" 6)}
wvSetPosition -win $_nWave3 {("G1" 6)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/DM1/DMout\[31:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 6 )} 
wvSetPosition -win $_nWave3 {("G1" 6)}
wvSetPosition -win $_nWave3 {("G1" 8)}
wvSetPosition -win $_nWave3 {("G1" 8)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 7 8 )} 
wvSetPosition -win $_nWave3 {("G1" 8)}
wvSetPosition -win $_nWave3 {("G1" 9)}
wvSetPosition -win $_nWave3 {("G1" 9)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/DM1/DMin\[31:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 9 )} 
wvSetPosition -win $_nWave3 {("G1" 9)}
wvGetSignalClose -win $_nWave3
wvSelectSignal -win $_nWave3 {( "G1" 7 )} 
wvSelectSignal -win $_nWave3 {( "G1" 6 )} 
wvGetSignalOpen -win $_nWave3
wvGetSignalSetScope -win $_nWave3 "/top_tb"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvGetSignalSetScope -win $_nWave3 "/top_tb/DM1"
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/WB_mux"
wvSetPosition -win $_nWave3 {("G1" 10)}
wvSetPosition -win $_nWave3 {("G1" 10)}
wvAddSignal -win $_nWave3 -clear
wvAddSignal -win $_nWave3 -group {"G1" \
{/top_tb/TOP/regfile1/Write_ADDR\[4:0\]} \
{/top_tb/TOP/regfile1/Write_Data\[31:0\]} \
{/top_tb/TOP/PC\[9:0\]} \
{/top_tb/TOP/regfile1/Write} \
{/top_tb/TOP/regfile1/Read} \
{/top_tb/DM1/DMout\[31:0\]} \
{/top_tb/DM1/enable_fetch} \
{/top_tb/DM1/enable_writeback} \
{/top_tb/DM1/DMin\[31:0\]} \
{/top_tb/TOP/WB_mux/src3\[31:0\]} \
}
wvAddSignal -win $_nWave3 -group {"G2" \
}
wvSelectSignal -win $_nWave3 {( "G1" 10 )} 
wvSetPosition -win $_nWave3 {("G1" 10)}
wvGetSignalSetScope -win $_nWave3 "/top_tb/TOP/regfile1"
wvSetCursor -win $_nWave3 57612.636503 -snap {("G2" 0)}
wvSelectSignal -win $_nWave3 {( "G1" 3 )} 
wvSetCursor -win $_nWave3 67806.199182 -snap {("G1" 3)}
wvSetCursor -win $_nWave3 78600.797397 -snap {("G1" 5)}
