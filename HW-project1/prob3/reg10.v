
module reg10 ( clk, rst, en_reg, d_in, d_out );
    input clk, rst, en_reg;
    input[9:0]	d_in;
    output[9:0] d_out;
    reg [9:0] d_out;
   
    always @( posedge clk ) begin
        if ( rst )
			d_out = 10'b0;
        else if ( en_reg )
			d_out = d_in;
    end

endmodule
	
