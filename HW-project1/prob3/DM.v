module DM ( clk, rst, enable_fetch, enable_writeback, enable_mem, DMin, DMout, DM_address ) ;

  parameter data_size = 32 ;
  parameter mem_size  = 4096 ;

  input clk, rst, enable_fetch, enable_writeback, enable_mem ;
  input [ data_size - 1 : 0 ]  DMin ;
  input [ 11 : 0 ] DM_address ;
  
  output [ data_size - 1 : 0 ] DMout ;

  reg [ data_size - 1 : 0 ] DMout ;

  reg [ data_size - 1 : 0 ] mem_data[ mem_size - 1 : 0 ] ;

  integer i ;


  always@( posedge clk ) begin
    if ( rst ) begin 
      for ( i = 0 ; i < mem_size ; i = i + 1 ) 
        mem_data[ i ] = 0 ;
      DMout = 0 ;  
    end
    
    else if ( enable_mem ) begin
      if ( enable_fetch ) begin
        DMout = mem_data[ DM_address ] ;
      end
      else if ( enable_writeback ) begin
        mem_data[ DM_address ] = DMin ;
      end 
    end
  end 

endmodule 