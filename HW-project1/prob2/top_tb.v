`timescale 1ns/10ps
`include "top.v"
//`define MINS "mins.prog"

module top_tb;
  reg clk,rst;  
//reg [31:0] instruction;
  wire alu_overflow;
  
  //reg [31:0] mem_data [19:0];
  integer i;

  top TOP(.clk( clk ), .rst( rst ), .alu_overflow( alu_overflow ));
  
  //clock gen.
  always #5 clk=~clk;
  initial begin : prog_load
  #10 $readmemb("mins.prog",TOP.IM1.mem_data);
	 //$readmemb("tb_code1.prog",TOP.IM1.mem_data);
	 //$readmemb("tb_code2.prog",TOP.IM1.mem_data);
  end
  
  initial begin
  clk=0;
  rst=1'b1;
  
  #10 rst=1'b0; 
      //Design your own test pattern 
      //Note that the time you send a new instruction depends on when the previous instruction is carried out.
 
  #600 $display( "done" );
  #40  rst=1'b1;
  //test & debug block
  //display register file contents to verify the results
  for( i=0;i<32;i=i+1 ) $display( "register[%d]=%d",i,TOP.regfile1.rw_reg[i] ); 
  #100 $finish;
  end
  initial begin
     $dumpfile( "top.vcd" ) ;
     $dumpvars; 
  end
  
endmodule
  
  
