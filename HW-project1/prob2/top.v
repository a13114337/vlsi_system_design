`include "ALU.v"
`include "IM.v"
`include "Mux.v"
`include "reg10.v"
`include "controller.v"
`include "RegFile.v"

module top ( clk, rst, alu_overflow ) ;
    input clk, rst ;
    
    output alu_overflow ;

    wire enable_fetch_inst, enable_fetch_decode, enable_execute, enable_writeback, mux2to1_select, imm_reg_select ;
    wire [ 1 : 0 ] mux4to1_select ;
    wire [ 4 : 0 ] sub_opcode, writeback_addr  ;
    wire [ 31 : 0 ] data1, data2, writeback_data, ExToAlu, RfToAlu, alu_result ;
    wire [ 31 : 0 ]  instruction ;
    wire [ 9 : 0 ] PC, PC_next ;
    wire pc_overflow ;

    PC_adder pc_adder( .enable( enable_fetch_decode ), .reslt( PC_next ), .src1( PC ), .src2( 10'b1 ) ) ;

    reg10 reg_PC ( .clk( clk ), .rst( rst ), .en_reg( 1'b1 ), .d_in( PC_next ), .d_out( PC ) ) ;

    IM IM1 ( .clk( clk ), .rst( rst ), .IM_address( PC ), .enable_fetch( enable_fetch_inst ), .enable_mem( 1'b1), .enable_write( 1'b0 ), .IMin( 32'b0 ), .IMout( instruction ) ) ;

    controller ctrl_unit ( .enable_fetch_inst( enable_fetch_inst ), .enable_execute( enable_execute ), .enable_fetch_decode( enable_fetch_decode ), .enable_writeback( enable_writeback ), 
    	                   .sub_opcode( sub_opcode ), .mux4to1_select( mux4to1_select), .mux2to1_select( mux2to1_select ), 
    	                   .imm_reg_select( imm_reg_select ), .clk( clk ), .rst( rst ), .ir( instruction ) ) ;

    
    ALU alu ( .enable_execute( enable_execute ), .OverFlow( alu_overflow ), .alu_result( alu_result ), .src1( data1 ), .src2( data2 ), .OP( sub_opcode ) ) ;


    RegFile regfile1 (  .clk( clk ), .rst( rst ), .Write( enable_writeback ), .Read( enable_fetch_decode ), .Write_ADDR( instruction[ 24 : 20 ] ), 
    	          .Read_ADDR1( instruction[ 19 : 15 ] ), .Read_ADDR2( instruction[ 14 : 10 ] ), .Write_Data( writeback_data ), .Read_Data1( data1 ), 
    	          .Read_Data2( RfToAlu ) ) ;

    mux2to1 imm_reg_mux( .select( imm_reg_select ), .Write_Data1( RfToAlu ), .Write_Data2( ExToAlu ), .Output_Data( data2 ) ) ;

    mux2to1 writeback_mux( .select( mux2to1_select ), .Write_Data1( data2 ), .Write_Data2( alu_result ), .Output_Data( writeback_data ) ) ;

    mux4to1 extension_mux( .select( mux4to1_select ), .Input_data( instruction[ 19 : 0 ] ), .Output_Data( ExToAlu )  ) ;

endmodule
