
`include "ALU.v"
`include "controller.v"
`include "Mux.v"
`include "RegFile.v"
`include "reg10.v"

module top ( clk, rst,
                 instruction,
                 IM_read,
             IM_write,
             IM_enable,
             IM_address,
             
             DM_out,
             DM_read,
             DM_write,
             DM_enable,
             DM_in,
             DM_address);

    input clk, rst ;
    input [ 31 : 0 ] instruction, DM_out ;


    output IM_read, IM_enable, IM_write , 
           DM_read, DM_enable, DM_write ;
           
    output [ 31 : 0 ] DM_in ;
         
      output[ 9 : 0 ] IM_address ;
    output[ 11 : 0 ] DM_address ;

    wire enable_fetch_inst, enable_fetch_DM,  enable_fetch_decode, 
         enable_execute,    enable_write_RF,  enable_write_DM, 
         enable_shift,      zero,             enable_branch,  Equal_select, branch,
         enable_jump,       enable_write_PC ;
         
    wire [ 1 : 0 ]  extend_mux_select, writeback_mux_select, SV, imm_reg_select ;
    
    wire [ 4 : 0 ]  sub_opcode ;
    
    wire [ 31 : 0 ] data1,   data2,   write_data_Mux_RF, 
                    ExToAlu, RfToAlu, alu_result, write_data_RF_DM, MuxToShift ;
                    
    wire [ 9 : 0 ]  PC, PC_next, branch_addr, PC_finial, PC_alu_result, Jump_result ;
    
    assign IM_address = PC ;
    assign IM_read = enable_fetch_inst ; 
    assign IM_enable = 1'b1 ;
    assign IM_write = 1'b0 ;
    
    
    assign DM_read = enable_fetch_DM ;
    assign DM_write = enable_write_DM ;
    assign DM_enable = 1'b1 ;
    assign DM_in = write_data_RF_DM ;
    assign DM_address = alu_result[ 11 : 0 ] ;

    assign data2 = ( enable_shift ) ? MuxToShift << SV : MuxToShift ; 


    PC_adder pc_adder( .enable( 1'b1 ), .reslt( PC_next ), 
                       .src1( PC ),                    .src2( 10'b1 ) ) ;
                       

    reg10 reg_PC ( .clk( clk ),      .rst( rst ),      .en_reg( enable_write_PC ), 
                   .d_in( PC_finial ), .d_out( PC ) ) ;


    controller ctrl_unit ( .enable_fetch_inst( enable_fetch_inst ), .enable_fetch_decode( enable_fetch_decode ), .enable_execute( enable_execute ), 
                           .enable_write_RF( enable_write_RF ),     .enable_write_DM( enable_write_DM ),         .enable_read_DM( enable_fetch_DM ),
                           .sub_opcode( sub_opcode ),               .extend_mux_select( extend_mux_select ),     .writeback_select( writeback_mux_select ),
                           .imm_reg_select( imm_reg_select ),       .clk( clk ), .rst( rst ),                    .ir( instruction ), 
                           .enable_shift( enable_shift ),           .SV( SV ),                                   .Branch_select( Equal_select ),
                           .enable_branch( branch ),                .enable_jump( enable_jump ),                 .enable_write_PC( enable_write_PC ) ) ;


    ALU alu ( .enable_execute( enable_execute ), 
              .OverFlow( alu_overflow ), 
              .alu_result( alu_result ), 
              .src1( data1 ),   
              .src2( data2 ),
              .OP( sub_opcode ),
              .zero( zero ) ) ;


    RegFile regfile1 (  .clk( clk ), .rst( rst ),              .Write( enable_write_RF ),             .Read( enable_fetch_decode ),  
                        .Write_ADDR( instruction[ 24 : 20 ] ), .Read_ADDR1( instruction[ 19 : 15 ] ), .Read_ADDR2( instruction[ 14 : 10 ] ),
                        .Read_ADDR3( instruction[ 24 : 20 ] ), .Write_Data( write_data_Mux_RF ),      .Read_Data1( data1 ),
                        .Read_Data2( RfToAlu ),                .Read_Data3( write_data_RF_DM ) ) ;

    mux3to1 imm_reg_mux( .select( imm_reg_select ), .src1( RfToAlu ), 
                         .src2( ExToAlu ),          .src3( write_data_RF_DM ),
                         .Output_Data( MuxToShift ) ) ;

    mux3to1 WB_mux( .select( writeback_mux_select ), .src1( data2 ), 
                    .src2( alu_result ),             .src3( DM_out ), 
                    .Output_Data( write_data_Mux_RF ) ) ;

    Sign_Extend_Mux extened_mux( .select( extend_mux_select ), .Input_data( instruction[ 19 : 0 ] ), .Output_Data( ExToAlu )  ) ;

    Equal_Mux  equal_mux( .select( Equal_select ), .zero( zero ), .branch( branch ), .Output( enable_branch ) ) ;

    assign PC_alu_result = PC_next + ( instruction[13:0] >> 2 )  ;

    assign Jump_result = PC_next + ( instruction[23:0] >> 2 ) ;   
     
    Address_Mux branch_mux( .select( enable_branch ), .address1( PC_next ), .address2( PC_alu_result ), .Output( branch_addr ) ) ;

    Address_Mux jump_mux( .select( enable_jump ), .address1(  branch_addr ), .address2( Jump_result ), .Output( PC_finial ) ) ;

endmodule
