module mux3to1 ( select, src1, src2, src3, Output_Data ) ;

  input [ 1 : 0 ] select ;
  input [ 31 : 0 ]  src1, src2, src3 ;
  output [ 31 : 0 ] Output_Data ;

  reg   [ 31 : 0 ] Output_Data ;
   
  always@( select or src1 or src2 or src3 ) begin 
    case ( select ) 
      2'b00 : Output_Data = src1 ;
      2'b01 : Output_Data = src2 ;
      2'b10 : Output_Data = src3 ;  
    endcase
  end

endmodule


module Sign_Extend_Mux( select, Input_data, Output_Data ) ;
  parameter DSize = 32 ;
  
  input [ 1 : 0 ] select ;
  input [ 19 : 0 ] Input_data ;
  output [ 31 : 0 ] Output_Data ;

  reg [ 31 : 0 ] Output_Data ;


  always@( select or Input_data ) begin
    case( select ) 
      2'b00 : begin
        Output_Data = { 27'd0, Input_data[ 14 : 10 ] } ;
      end

      2'b01 : begin
        Output_Data = { 17'd0, Input_data[ 14 : 0 ] } ;
      end 

      2'b10 : begin
        Output_Data = { 17'd0, Input_data[ 14 : 10 ] } ;
      end 

      2'b11 : begin
        Output_Data = { 12'd0, Input_data[ 19 : 0 ] } ;
      end
    endcase  
  end

endmodule

module Equal_Mux( select, zero, branch, Output ) ;

  input select, zero, branch ;
  output Output ;
  reg Output ;

  always@( * ) begin
    if ( select == 1'b0 ) begin
      Output = zero & branch ;
    end
    else begin
      Output = ( zero ^ branch ) & branch ;
    end
  end 

endmodule

module Address_Mux( select, address1, address2, Output ) ;
  input select ;
  input [ 9 : 0 ] address1, address2 ;
  output [ 9 : 0 ] Output ;  
  reg [ 9 : 0 ] Output ;

  always@( * ) begin
    if ( select == 1'b0 ) 
      Output = address1 ;
    else 
      Output = address2 ;
  end

endmodule