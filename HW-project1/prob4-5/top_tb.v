`timescale 1ns/10ps
`include "IM.v"
`include "DM.v"
`include "top.v"
`define prog3


module top_tb;

  reg clk;
  reg rst;
  integer i;
  wire [31:0]ir;

  //IM
  wire IM_read, IM_write, IM_enable;
  wire [9:0] IM_address;
  
  //DM
  wire DM_read ,DM_enable;
  wire DM_write;
  wire [31:0] DM_in;
  wire [11:0] DM_address;
  wire [31:0] DMout;
 
  top TOP(.clk(clk),
  			  .rst(rst),
  			  .instruction(ir),
  			  .IM_read(IM_read),
          .IM_write(IM_write),
          .IM_enable(IM_enable),
          .IM_address(IM_address),
  			  .DM_out(DMout),
          .DM_read(DM_read),
          .DM_write(DM_write),
          .DM_enable(DM_enable),
          .DM_in(DM_in),
          .DM_address(DM_address)); 
 
  DM DM1(.clk(clk),
  			 .rst(rst),
  			 .enable_fetch(DM_read),
  			 .enable_writeback(DM_write),
  			 .enable_mem(DM_enable),
  			 .DMin(DM_in),
  			 .DMout(DMout),
  			 .DM_address(DM_address));

  IM IM1(.clk(clk),
  			 .rst(rst),
  			 .IM_address(IM_address),
  			 .enable_fetch(IM_read),
  			 .enable_write(IM_write),
  			 .enable_mem(IM_enable),
  			 .IMin(),
  			 .IMout(ir)); 
  
  
  
  //clock gen.
  always #5 clk=~clk;
  
  
 initial begin
  clk=0;
  rst=1'b1;
  #20 rst=1'b0;

  `ifdef prog1
  		  //verification program 1
  			$readmemb("mins.prog",IM1.mem_data); 
  			$readmemb("data.prog"",DM1.mem_data); 
  `endif
  
  `ifdef prog2
  		  //verification program 2
  			$readmemb("mins.prog",IM1.mem_data);
  			$readmemb("data.prog"",DM1.mem_data); 
  `endif
  
  `ifdef prog3
  		  //verification program 3
  			$readmemb("mins.prog",IM1.mem_data);
  			//$readmemb("data.prog",DM1.mem_data); 
  `endif

       
  	#15000
      $display( "done" );
      for( i=0;i<31;i=i+1 ) $display( "IM[%h]=%h",i,IM1.mem_data[i] ); 
      for( i=0;i<32;i=i+1 ) $display( "register[%d]=%d",i,TOP.regfile1.rw_reg[i] ); 
      for( i=0;i<40;i=i+1 ) $display( "DM[%d]=%d",i,DM1.mem_data[i] );
            
      $finish;
  end
  
  initial begin
     $dumpfile( "top.vcd" ) ;
     $dumpvars; 
  end
endmodule
