

module controller ( enable_fetch_inst, enable_fetch_decode, enable_execute, enable_write_RF, 
                    enable_write_DM,   enable_read_DM,      sub_opcode, 
	                  extend_mux_select, writeback_select,    imm_reg_select, clk, rst, ir,
                    enable_shift, SV,  Branch_select,       enable_branch,  enable_jump,
                    enable_write_PC ) ;

    input   clk, rst ;
    input   [ 31 : 0 ] ir ;

    output  enable_fetch_inst, 
            enable_fetch_decode, 
            enable_execute, 
            enable_write_RF,  
            enable_write_DM,
            enable_read_DM,
            enable_shift,
            Branch_select,
            enable_branch,
            enable_jump,
            enable_write_PC ;

    output  [ 4 : 0 ] sub_opcode ;
    output  [ 1 : 0 ] extend_mux_select, writeback_select, SV, imm_reg_select ;
    

    reg     enable_fetch_inst, 
            enable_fetch_decode, 
            enable_execute, 
            enable_write_RF, 
            enable_write_DM_reg,
            enable_write_DM,
            enable_read_DM,
            enable_shift,
            Branch_select,
            enable_branch,
            enable_jump,
            enable_write_PC ;

    reg     [ 1 : 0 ] extend_mux_select, writeback_select, SV, imm_reg_select ;
    reg     [ 1 : 0 ] current_state, next_state ;
    reg     [ 4 : 0 ] sub_opcode ;

    wire    [ 5 : 0 ] opcode = ir[ 30 : 25 ] ;
    

    parameter S0 = 2'b00, S1 = 2'b01, S2 = 2'b10, S3 = 2'b11 ;

    always@( posedge clk ) begin 
      if ( rst )  current_state = S0 ;
      else current_state = next_state ;
    end

    always@(  posedge clk or current_state ) begin
      case ( current_state ) 
        S0 : begin
          next_state = S1 ;
          enable_fetch_inst = 1'b1 ;
          enable_fetch_decode = 1'b0 ;
          enable_execute = 1'b0 ;
          enable_write_RF = 1'b0 ;
          enable_read_DM = 1'b0 ;
          enable_write_DM = 1'b0 ;
          enable_write_DM_reg = 1'b0 ;
          enable_shift = 1'b0 ;
          writeback_select = 2'b0 ;
          imm_reg_select = 2'b0 ;
          extend_mux_select = 2'b0 ;
          SV = ir[ 9 : 8 ] ;
          Branch_select = 1'b0 ; 
          enable_branch = 1'b0 ;
          enable_jump = 1'b0 ;
          enable_write_PC = 1'b0 ;
        end

        S1 : begin
          next_state = S2 ;
          enable_fetch_inst = 1'b0 ;
          enable_fetch_decode = 1'b1 ;
        end

        S2 : begin
          next_state = S3 ;
          enable_fetch_decode = 1'b0 ;
          enable_execute = 1'b1 ;
          case ( opcode )
            6'b100000 : begin  // R-Type  
              writeback_select = 2'b1 ;
              sub_opcode = ir[ 4 : 0 ] ;
              if ( ir[ 24: 10 ] == 15'b0 )  begin
                next_state = S0;
                enable_write_PC = 1'b1 ;
              end             
              if ( sub_opcode[4:3] == 2'b00 ) begin  // use reg
                imm_reg_select = 2'b0 ;
              end  
              
              else begin  //  use imm value
                imm_reg_select = 2'b01 ;
                extend_mux_select = 2'b00 ;        
              end  
            end  

            6'b101000 : begin  // ADDI
              imm_reg_select = 2'b01 ;   
              extend_mux_select = 2'b01 ;  // 15bits SE
              writeback_select = 2'b1 ;
              sub_opcode = 5'b00000 ;

            end

            6'b101100 : begin  // ORI
              imm_reg_select = 2'b01 ;   
              extend_mux_select = 2'b01 ;  // 15bits ZE
              writeback_select = 2'b1 ;
              sub_opcode = 5'b00100 ;
            end

            6'b101011 : begin  // XORI
              imm_reg_select = 2'b01 ;   
              extend_mux_select = 2'b01 ;  // 15bits ZE
              writeback_select = 2'b1 ;
              sub_opcode = 5'b00011 ;
            end

            6'b000010 : begin  // LWI
              imm_reg_select = 2'b01 ;
              extend_mux_select = 2'b01 ;
              enable_shift = 1'b1 ;
              sub_opcode = 5'b00000 ;
              SV = 2'b10 ;
              enable_read_DM = 1'b1 ;
              writeback_select = 2'b10 ;
              enable_write_DM_reg = 1'b0 ;
            end

            6'b001010 : begin  // SWI
              imm_reg_select = 2'b01 ;
              extend_mux_select = 2'b01 ;
              enable_shift = 1'b1 ;
              sub_opcode = 5'b00000 ;
              SV = 2'b10 ;
              enable_read_DM = 1'b0 ;
              enable_write_DM_reg = 1'b1 ;
            end

            6'b100010 : begin  // MOVI
              imm_reg_select = 2'b01 ;   
              extend_mux_select = 2'b11 ;  // 15bits ZE
              sub_opcode = 5'b01001 ;
              writeback_select = 2'b0 ;
            end

            6'b011100 : begin  // LW & SW
              imm_reg_select = 2'b0 ;
              enable_shift = 1'b1 ;
              sub_opcode = 5'b00000 ;
              SV = ir[ 9 : 8 ] ;         
              if ( ir[ 7 : 0 ] == 8'b00000010 ) begin  // LW
                enable_read_DM = 1'b1 ;
                writeback_select = 2'b10 ;
                enable_write_DM_reg = 1'b0 ;
              end

              else if ( ir[ 7 : 0 ] == 8'b00001010 ) begin  // SW
                enable_write_DM_reg = 1'b1 ;
              end
            end

            6'b100110 : begin // branch
              Branch_select = ir[ 14 ] ;
              enable_branch = 1'b1 ;
              sub_opcode = 5'b00001 ;
              imm_reg_select = 2'b10 ;
            end
 
            6'b100100 : begin  // Jump
              enable_jump = 1'b1 ;
            end
            default : begin
              next_state = S0;
              enable_write_PC = 1'b1 ;
            end
          endcase
        end

         S3 : begin
          next_state = S0 ;
          enable_execute = 1'b0 ;
          enable_write_PC = 1'b1 ;
          if ( ( enable_jump | enable_branch ) == 1'b0 ) begin
            if ( enable_write_DM_reg == 1'b0 )
              enable_write_RF = 1'b1 ;
            else
              enable_write_DM = 1'b1;
          end

        end

        default : $display( "Invalid state" ) ;
      endcase    
    end
endmodule
