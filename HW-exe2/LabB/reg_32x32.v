module reg_32x32 ( OUT_1, OUT_2, Write, Read, Read_ADDR_1, Read_ADDR_2, Write_ADDR, DIN, enable, clk, rst ) ;
  
  parameter ADSize = 5 ;    // 5 bits
  parameter REGSize = 32 ;  // 32 bits  
  parameter DASize = 32 ;   // 32 bits 

  input clk, rst, enable ;
  input [ ADSize - 1 : 0 ] Read_ADDR_1, Read_ADDR_2, Write_ADDR ;
  input [ DASize - 1 : 0 ] DIN ;
  input Write ;
  input Read ;
  output [ DASize - 1 : 0 ] OUT_1, OUT_2 ;
   

  reg [ DASize - 1 : 0 ] OUT_1, OUT_2 ;
  
  reg [ DASize - 1 : 0 ] RF [ REGSize -1 : 0 ] ;

  integer i ;

  always@( posedge clk or posedge rst ) begin
    if ( rst )
      for ( i = 0 ; i < REGSize ; i = i + 1 ) 
        RF[i] <= 0 ; 
    else 
      if ( enable )
        if ( Write ) 
          RF[Write_ADDR] <= DIN ;
        else if ( Read ) begin
          OUT_1 <= RF[Read_ADDR_1] ;
          OUT_2 <= RF[Read_ADDR_2] ;
        end 
  end  

endmodule