module RFILE2 ( enable, clk, rst, Read, Write, DIN, Read_ADDR, Write_ADDR, OUT_1, OUT_2, OUT_3 ) ;


  parameter ADSize = 5 ;    // 5 bits
  parameter STP_REGSize = 32 ;  // 32 bits  
  parameter DASize = 32 ;   // 32 bits 

  input clk, rst, enable ;
  input [ ADSize - 1 : 0 ] Read_ADDR, Write_ADDR ;
  input [ DASize - 1 : 0 ] DIN ;
  input Write, Read ;
  output [ DASize - 1 : 0 ] OUT_1, OUT_2, OUT_3 ;


  reg [ DASize - 1 : 0 ] OUT_1, OUT_2, OUT_3 ;
  reg [ DASize - 1 : 0 ] Mreg[ STP_REGSize - 1 : 0 ] ;

  integer i ;
  always@( posedge clk or posedge rst ) begin
    if ( rst ) begin
      for ( i = 0 ; i < 32 ; i = i + 1 ) 
        Mreg[i] <= 0 ; 
    end    
    else 
      if ( enable )
        if ( Write ) 
          Mreg[Write_ADDR] <= DIN ;
        else if ( Read ) begin
          OUT_1 <= Mreg[Read_ADDR] ;
          OUT_2 <= Mreg[Read_ADDR+1] ;
          OUT_3 <= Mreg[Read_ADDR+2] ;
        end 
  end  

endmodule