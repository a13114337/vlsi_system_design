module ALU ( OverFlow, alu_result, src1, src2, OP ) ;
   parameter ISize = 32 ;
   parameter OPSize = 4 ;

   
   input [ ISize - 1 : 0 ] src1 ;
   input [ ISize - 1 : 0 ] src2 ;
   input [ OPSize - 1 : 0 ] OP ;// operation code 
   output [ ISize - 1 : 0 ] alu_result ;
   output OverFlow ;

   reg [ ISize - 1 : 0 ] alu_result ;
   reg [ 2 * ISize - 1 : 0 ] temp ;
   reg OverFlow ;
   integer i  ;

   always@( OP or src1 or src2 ) begin
     OverFlow = 0 ;
       case ( OP )
         4'b0000 :
         begin
           alu_result = src1 + src2 ;// ADD
           if ( ( alu_result[31] == 0 && src1[31] == 1 && src2[31] == 1 ) ||
                ( alu_result[31] == 1 && src1[31] == 0 && src2[31] == 0 ) )
             OverFlow = 1 ;
           else
             OverFlow = 0 ;
         end
      
         4'b0001:
         begin
           alu_result = src1 - src2 ;// sub
           if ( ( alu_result[31] == 0 && src1[31] == 1 && src2[31] == 0 ) ||
                ( alu_result[31] == 1 && src1[31] == 0 && src2[31] == 1 ) )
             OverFlow = 1 ;
           else
             OverFlow = 0 ;
         end

         4'b0010: alu_result = src1 & src2 ;// and
         
         4'b0011: alu_result = src1 | src2 ;// or
         
         4'b0100: alu_result = src1 << src2 ;// left shift       
         
         4'b0101: 
         begin 
            temp = { src2, src2 } ;
            
            for ( i = src1 ; i > 0 ; i = i - 32  ) begin
              if ( i < 32 ) temp = temp >> i ;
            end  
         end

         default : alu_result = alu_result ; 
       endcase
   end  

endmodule