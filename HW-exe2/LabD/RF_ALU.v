`include "ALU.v" 
`include "RegFile.v"
`include "Mux.v"


module RF_ALU( clk, rst, Select, Write, Read, OP, Read_ADDR1, Read_ADDR2, Write_ADDR, Write_Data, OverFlow ) ;
  parameter ADSize = 5 ;    // 5 bits
  parameter REGSize = 32 ;  // 32 bits  
  parameter DASize = 32 ;   // 32 bits 
  parameter OPSize = 4 ;

  input clk, rst, Select, Write, Read ;
  input [ OPSize - 1 : 0 ] OP ;
  input [ DASize - 1 : 0 ] Write_Data ; 
  input [ ADSize - 1 : 0 ] Read_ADDR1, Read_ADDR2, Write_ADDR ;
  output OverFlow ;

  
  wire [31:0] ALU_input1, ALU_input2, ALU_Result, Mux_Write_Data ,Write_Data;
  wire Select;
  
  Mux mux ( .Select( Select ), .Write_Data1( Write_Data ), .Write_Data2( ALU_Result ), .Output_Data( Mux_Write_Data ) ) ;
  

  RegFile regfile (  .clk( clk ), .rst( rst ), .Write( Write ), .Read( Read ), .Write_ADDR( Write_ADDR ), .Read_ADDR1( Read_ADDR1 ),
  	                 .Read_ADDR2( Read_ADDR2 ), .Write_Data( Mux_Write_Data ), .Read_Data1( ALU_input1 ), .Read_Data2( ALU_input2 ) ) ;
  
  
  ALU alu (  .OverFlow( OverFlow ), .alu_result( ALU_Result ), .src1( ALU_input1 ), .src2( ALU_input2 ), .OP( OP ) ) ;





endmodule