module Mux( Select, Write_Data1, Write_Data2, Output_Data ) ;
  parameter DSize = 32 ;

  input Select ;
  input [ DSize -1 : 0 ] Write_Data1, Write_Data2 ;
  output[ DSize - 1 : 0 ] Output_Data ;

  reg [ DSize - 1 : 0 ] Output_Data ;

  always@( Select or Write_Data1 or Write_Data2 ) begin
    if ( !Select )
      Output_Data <= Write_Data1 ;
    else 
      Output_Data <= Write_Data2 ;  
  end

endmodule