`timescale 1ns/10ps

`include "RF_ALU.v"

`define cycle 10

module tb;


parameter ADSize = 5 ;    // 5 bits
parameter REGSize = 32 ;  // 32 bits  
parameter DASize = 32 ;   // 32 bits 
parameter OPSize = 4 ;


reg clk, rst, Select, Write, Read ;
reg [ OPSize - 1 : 0 ] OP ;
reg [ DASize - 1 : 0 ] Write_Data ; 
reg [ ADSize - 1 : 0 ] Read_ADDR1, Read_ADDR2, Write_ADDR ;

integer i;

RF_ALU R1( clk, rst, Select, Write, Read, OP, Read_ADDR1, Read_ADDR2, Write_ADDR, Write_Data, OverFlow );


always #(`cycle/2) clk=~clk;

initial
begin

clk=0;
rst=1'b0;
#4;
rst=1'b1;
#`cycle;

rst=1'b0;
#`cycle;

#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b1_0_00000_00000_00000,32'd4,5'b0_0000};
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b1_0_00000_00000_00001,32'd3,5'b0_0000};
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b0_1_00000_00001_00000,32'd4,5'b0_0001};//R2=R0-R1
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b1_0_00000_00000_00010,32'd4,5'b1_0001};
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b0_1_00000_00001_00000,32'd4,5'b0_0011};//R3=R0|R1
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b1_0_00000_00000_00011,32'd4,5'b1_0011};
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b0_1_00011_00010_00000,32'd4,5'b0_0100};//R4=R3<<R2
#`cycle;
{Write,Read,Read_ADDR1,Read_ADDR2,Write_ADDR,Write_Data,Select,OP}={17'b1_0_00000_00000_00100,32'd4,5'b1_0100};

#`cycle;
	
$finish;

end


endmodule
