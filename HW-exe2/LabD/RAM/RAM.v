// RAM

module RAM (clk, rst, en_read, en_write, addr, DMin, DMout);

	input clk, rst, en_read, en_write;
	input [3:0] addr;
	input [15:0] DMin; 
	
	output reg [15:0] DMout;
	
	reg [15:0] RAM_Data [15:0];

	integer i;
	
	always@(posedge clk or rst) begin
		if (rst) begin
			for (i=0 ; i<16 ; i=i+1)
				RAM_Data[i] <= 16'b0;
		end
		else if (en_read)
			DMout <= RAM_Data[addr];
		else if (en_write)
			RAM_Data[addr] <= DMin;
		else
			DMout <= 16'bz;
	end

endmodule