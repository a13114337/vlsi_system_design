// RAM_tb

`timescale 1ns/10ps
`include "RAM.v"

module RAM_tb;

	reg			clk, rst, enable_read, enable_write;
	reg [15:0]	DMin;
	reg [3:0]	address;
	reg [5:0]	i;
	wire [15:0]	DMout;

	RAM ram1(	.clk(clk), .rst(rst), 
				.en_read(enable_read), .en_write(enable_write), 
				.addr(address), .DMin(DMin), .DMout(DMout) );
	
	initial begin
		clk = 1'b0;
		rst = 1'b0;
	end
	
	always #10 clk=~clk;
	
	initial begin
		// Please modify this part to verify your module
		enable_read=0; enable_write=0; address=5'b0; DMin=32'd0;
		#20 enable_read=0; enable_write=0;
		#20 enable_write=1;
				address = 4'd0; DMin=16'h0;
		#20	address = 4'd2; DMin=16'h24;
		#20	address = 4'd4; DMin=16'h4;
		#20	address = 4'd6; DMin=16'h31;
		#20	address = 4'd7; DMin=16'h2;
		#20	address = 4'd9; DMin=16'h20;
		#20	address = 4'd10; DMin=16'h10;
		#20	address = 4'd12; DMin=16'h12;
		#20	address = 4'd15; DMin=16'h15;
			
		// display result
		#20 for (i=0 ; i<16 ; i=i+1)
				$display($time, " RAM[%d]=%h, ",i, ram1.RAM_Data[i]);
		rst = 1'b1;
		#40 rst = 1'b0;
		$display($time, " Reset Function Test");
		for (i=0 ; i<16 ; i=i+1)
				$display($time, " RAM[%d]=%h, ",i, ram1.RAM_Data[i]);
		#40 $finish;
	end
	
	initial begin
		$dumpfile("RAM_tb.vcd");
		$dumpvars;
	end
	
endmodule