`timescale 1ns/10ps
`include "mux2to1.v"

module mux4to1_tb;
  reg S0, S1, I0, I1, I2, I3;
  wire Y;

  mux4to1 m0(.Y(Y), .S0(S0), .S1(S1), .I0(I0), .I1(I1), .I2(I2), .I3(I3) );
  initial begin
    $monitor( $time, " S1=%d,S0=%d,I0=%d,I1=%d,I2=%d,I3=%d,Y=%d", S1, S0, I0, I1, I2, I3, Y );
  end
  
  initial begin
        S1=0;S0=0;I0=0;I1=0;I2=0;I3=0;
    #10           I0=0;I1=0;I2=0;I3=1;
    #10           I0=0;I1=0;I2=1;I3=0;
    #10           I0=0;I1=0;I2=1;I3=1;
    #10 S1=0;S0=1;I0=0;I1=1;I2=0;I3=0;
    #10           I0=0;I1=1;I2=0;I3=1;
    #10           I0=0;I1=1;I2=1;I3=0;
    #10           I0=0;I1=1;I2=1;I3=1;
    #10 S1=1;S0=0;I0=1;I1=1;I2=0;I3=0;
    #10           I0=1;I1=0;I2=0;I3=1;
    #10           I0=1;I1=0;I2=1;I3=0;
    #10           I0=1;I1=0;I2=1;I3=1;
    #10 S1=1;S0=1;I0=1;I1=1;I2=0;I3=0;
    #10           I0=1;I1=1;I2=0;I3=1;
    #10           I0=1;I1=1;I2=1;I3=0;
    #10           I0=1;I1=1;I2=1;I3=1;
    
  end
  
  initial begin
      $dumpfile( "mux4to1.vcd" );
      $dumpvars;
      #400 $finish;
  end
endmodule
