module one_bit_fulladder( S, cout, A, B, cin );

  input A, B, cin;
  output S, cout;
  
  wire y1, y2, y3;
  
  xor(S, y1, cin);
  or(cout, y2, y3);
  
  xor(y1, A, B);
  
  and(y2, y1, cin);
  
  and(y3, A, B);
  
endmodule
