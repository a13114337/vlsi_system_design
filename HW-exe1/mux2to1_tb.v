`timescale 1ns / 10ps
`include "mux2to1.v"

module mux2to1_tb ;
  reg S, I0, I1 ; // inputs
  wire Y ; // outputs
  mux2to1 m0 ( .Y(Y), .S(S), .I0(I0), .I1(I1) ) ;
  initial begin
    $monitor( $time,"S=%d, I0=%d, I1=%d, Y=%d", S, I0,I1,Y ) ;
  end
  
  initial begin
        S=0; I0=0;I1=0;
    #10      I0=0;I1=1;
    #10      I0=1;I1=0;
    #10      I0=1;I1=1;
    #10 S=1; I0=0;I1=0;
    #10      I0=0;I1=1;
    #10      I0=1;I1=0;
    #10      I0=1;I1=1;
  end

  initial begin
    $dumpfile( "mux2to1.vcd") ;
    $dumpvars;
    #200 $finish;
  end
endmodule  
  
  
  