module FA4(S,C4,A,B,C0);
  input[3:0]   A,B;
  input    C0;
  output[3:0]  S;
  output   C4;
  
  wire            C1,C2,C3,C4;
  
  one_bit_fulladder A0(.S(S[0]),.cout(C1),.A(A[0]),.B(B[0]),.cin(C0));

  one_bit_fulladder A1(.S(S[1]),.cout(C2),.A(A[1]),.B(B[1]),.cin(C1));

  one_bit_fulladder A2(.S(S[2]),.cout(C3),.A(A[2]),.B(B[2]),.cin(C2));

  one_bit_fulladder A3(.S(S[3]),.cout(C4),.A(A[3]),.B(B[3]),.cin(C3));
  
endmodule