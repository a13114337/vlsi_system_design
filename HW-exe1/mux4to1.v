module mux4to1(Y,S0,S1,I0,I1,I2,I3);
  input S0,S1,I0,I1,I2,I3;
  
  output Y;
  
  wire y0,y1;
  
  mux2to1 m0(.Y(y0), .S(S0), .I0(I0), .I1(I1));
  
  mux2to1 m1(.Y(y1), .S(S0), .I0(I2), .I1(I3));
  
  mux2to1 m2(.Y(Y), .S(S1), .I0(y0), .I1(y1));
  
endmodule
