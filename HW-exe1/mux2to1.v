module mux2to1( Y,S,I0,I1 ) ;
  output Y;
  input S, I0, I1 ;
  wire a,b,c ;
  or( Y,b,c ) ;
  and( b,a,I0 ) ;
  and( c , S, I1 ) ;
  not( a, S ) ;
endmodule